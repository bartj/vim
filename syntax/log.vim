" Vim syntax file
" Language: Multiple log file formats
" Maintainer: Bart Joy

if exists("b:current_syntax")
  finish
endif

"legacy log files
syn match legacystart '^\d\+,\d\+,\d\+,\d\d\d\d\/\d\d\/\d\dT\d\d:\d\d:\d\d\.\d\d,[^ !]\+!\s' contains=legacylogdate,legacykeyword
syn match legacylogdate '\d\d\d\d\/\d\d\/\d\dT\d\d:\d\d:\d\d\.\d\d' contained
syn match legacykeyword '[^ ,!]\+!' contained

"modern log files
syn match start '^\d\{4\}-\d\{2\}-\d\dT\d\d:\d\d:\d\d\.\d\d,[0-9a-fA-F]\{8\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{12\},[a-zA-Z]\+,' contains=logdate,activityid,keywordInformation,keywordWarning,keywordError
syn match logdate '^\d\{4\}-\d\{2\}-\d\dT\d\d:\d\d:\d\d\.\d\d' contained
syn match activityid '[0-9a-fA-F]\{8\}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{4}-[0-9a-fA-F]\{4\}-[0-9a-fA-F]\{12\}' contained
syn keyword keywordInformation InformationMilestone InformationGeneral InformationSqlQuery InformationPerformance InformationCalculationAudit SuccessOk contained
syn keyword keywordWarning ServerRecoveredFromProblem contained
syn keyword keywordError ClientInvalidFormat ClientInvalidData ClientSuspiciousData ClientBusinessRuleViolation ServerConnectivityProblem ServerSoftwareProblem ServerCatchallException ServerKnownException ServerBadDataException contained

syn match exceptionLine '^   at .*$'
syn match exceptionLine '^--- End of stack trace from previous location where exception was thrown ---$'
syn match timing '\d\+ µs'

let b:current_syntax = "log"

hi def link start Comment
hi def link legacystart Comment
hi def link logdate Constant
hi def link legacylogdate Constant
hi def link activityid Structure
hi def link keywordInformation Function
hi def link keywordWarning Identifier
hi def link keywordError Statement
hi def link legacykeyword Function

hi def link timing Macro
hi def link exceptionLine Comment

" function copied from http://vim.wikia.com/wiki/Different_syntax_highlighting_within_regions_of_a_file
function! TextEnableCodeSnip(filetype,start,end,textSnipHl) abort
  let ft=toupper(a:filetype)
  let group='textGroup'.ft
  if exists('b:current_syntax')
    let s:current_syntax=b:current_syntax
    " Remove current syntax definition, as some syntax files (e.g. cpp.vim)
    " do nothing if b:current_syntax is defined.
    unlet b:current_syntax
  endif
  execute 'syntax include @'.group.' syntax/'.a:filetype.'.vim'
  try
    execute 'syntax include @'.group.' after/syntax/'.a:filetype.'.vim'
  catch
  endtry
  if exists('s:current_syntax')
    let b:current_syntax=s:current_syntax
  else
    unlet b:current_syntax
  endif
  execute 'syntax region textSnip'.ft.'
  \ matchgroup='.a:textSnipHl.'
  \ start="'.a:start.'" end="'.a:end.'"
  \ contains=@'.group
endfunction

call TextEnableCodeSnip(  'xml',   'payload:\n',   '\n', 'Normal')
