"Git commit messages
au FileType gitcommit setlocal tw=72
au FileType gitcommit setlocal comments=fb:-,fb:*
au FileType gitcommit set formatoptions=tcqj
au FileType gitcommit set comments=fb:-,fb:*
au FileType gitcommit setlocal spell
if exists('$GIT_AUTHOR_DATE')
    finish
endif

"There's no built-in way to get the Vim home directory
if has('win32') || has ('win64')
    let $VIMHOME = $HOME."/vimfiles"
else
    let $VIMHOME = $HOME."/.vim"
endif

if !has("python3")
echo 'It appears that python is not correctly installed.'
finish
endif

if !has("lua")
echo 'It appears that LUA is not correctly installed.'
finish
endif

"vim-plug
call plug#begin($VIMHOME."/plugged")
Plug 'bling/vim-airline'
Plug 'Chiel92/vim-autoformat'
Plug 'chrisbra/csv.vim'
Plug 'dhruvasagar/vim-table-mode'
Plug 'dkarter/bullets.vim'
Plug 'easymotion/vim-easymotion'
Plug 'editorconfig/editorconfig-vim'
Plug 'godlygeek/tabular'
Plug 'kien/ctrlp.vim'
Plug 'liuchengxu/vista.vim'
Plug 'mattn/calendar-vim'
Plug 'mattn/emmet-vim/'
Plug 'mbbill/undotree'
Plug 'mhinz/vim-grepper'
Plug 'mhinz/vim-signify'
Plug 'mxw/vim-jsx'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'pangloss/vim-javascript'
Plug 'plasticboy/vim-markdown'
Plug 'PProvost/vim-ps1'
Plug 'rhysd/vim-grammarous'
Plug 'scrooloose/nerdcommenter'
Plug 'scrooloose/nerdtree'
Plug 'tomasr/molokai'
Plug 'tpope/vim-dispatch'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-jdaddy'
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-sensible'
Plug 'tpope/vim-surround'
Plug 'tpope/vim-vinegar'
Plug 'vim-scripts/argtextobj.vim'
Plug 'vimwiki/vimwiki'
Plug 'Xuyuanp/nerdtree-git-plugin'
call plug#end()

if !isdirectory($VIMHOME."/plugged")
    "This code is only executed on first load when we're installing plugins
    "automatically and don't want to execute any code that depends on plugins
    "because they have not yet been installed.
    finish
endif

"general
syntax on
filetype plugin indent on
colorscheme darkblue
set cmdheight=2
set hidden
set tabstop=4
set shiftwidth=4
set copyindent
set number
set ignorecase
set nofoldenable
set smartcase
set hlsearch
set expandtab
set list
set listchars=tab:>.,trail:.,extends:#,nbsp:.
set directory=$TEMP,~/tmp,/tmp
set scrolloff=10
set shortmess+=I
set splitbelow
set splitright
set nowrap
set spelllang=en_nz
set linebreak
set nojoinspaces
set wrap
set showbreak=…
noremap <silent> <tab> :bn<cr>
noremap <silent> <s-tab> :bp<cr>
let mapleader = ","

"r/rstudio settings
autocmd FileType r setlocal shiftwidth=2 tabstop=2

"Allow Vim to treat file names with line numbers such as blah:29 as such;
"otherwise it believes that the colon is not part of the file name since it
"could be a drive delimiter
"See: https://stackoverflow.com/questions/36500099/vim-gf-should-open-file-and-jump-to-line
set isfname-=:

"vim-table-mode
let g:table_mode_corner="|"

"editorconfig
let g:EditorConfig_exclude_patterns = ['fugitive://.*', 'scp://.*']

"allows renaming of identifiers; source: http://vim.wikia.com/wiki/Search_and_replace_the_word_under_the_cursor
nnoremap <Leader>s :%s/\<<C-r><C-w>\>/

"vim-grepper
runtime plugin/grepper.vim
let g:grepper.highlight = 1
let g:grepper.dir = 'repo,file,cwd'
let g:grepper.rg.grepprg .= ' --smart-case -g!/node_modules/'
"let g:grepper.side = 1         looks promising, but buggy when I tried it
let g:grepper.tools = ['rg', 'git', 'ag', 'grep', 'findstr']

" Put plugins and dictionaries in this dir (also on Windows)
let vimDir = '$HOME/.vim'
let &runtimepath.=','.vimDir

" Keep undo history across sessions by storing it in a file
if has('persistent_undo')
    let myUndoDir = expand(vimDir . '/undodir')
    " Create dirs
    call system('mkdir ' . vimDir)
    call system('mkdir ' . myUndoDir)
    let &undodir = myUndoDir
    set undofile
endif

"netrw / vinegar
augroup netrw_mappings
  autocmd!
  autocmd FileType netrw nmap <buffer> h -
  autocmd FileType netrw nmap <buffer> l <CR>
augroup END

"airline plugin
let g:airline#extensions#tabline#enabled = 1
"let g:airline_powerline_fonts = has('win32') ? 1 : 0   disabled until the issue with font rendering is resolved.
"See: https://github.com/vim-airline/vim-airline/issues/1291
"See: https://github.com/vim-airline/vim-airline/issues/820
"See: https://github.com/powerline/fonts/issues/169

"disable search highlighting by pressing escape
nnoremap <esc> :noh<return><esc>

"turn on spell-checking for certain file types
autocmd FileType mkd,text setlocal spell

"improve word breaks in text file types (note: this requires nolist, unfortunately)
autocmd FileType mkd,text setlocal nolist

"markdown preview in Chrome - requires Markdown Preview
"(https://chrome.google.com/webstore/detail/markdown-preview/jmchmkecamhbiokiopfpnfgbidieafmd)
autocmd BufEnter *.md,*.markdown exe 'noremap <F5> :!start C:\Program Files (x86)\Google\Chrome\Application\chrome.exe "%:p"<CR>'
set updatetime=200
augroup MarkdownAutoWrite
    autocmd FileType mkd :autocmd! MarkdownAutoWrite CursorHold,CursorHoldI <buffer> :update
augroup END
let g:vim_markdown_folding_disabled = 1

"ctrlp plugin
noremap <leader>p :CtrlP .<cr>
let g:ctrlp_max_files = 0
let g:ctrlp_map = '<c-p>'
let g:ctrlp_cmd = 'CtrlP'
let g:ctrlp_clear_cache_on_exit = 0
let g:ctrlp_user_command = ['.git', 'cd %s && rg --files-with-matches ".*" -g !/node_modules/ -g !/vendor/ -g !/ThirdParty/ -g !/bower_components/', 'find %s -type f']
let g:ctrlp_custom_ignore = {
    \ 'dir':  '\v[\/](\.(git|hg|svn)|bin|obj|node_modules|vendor|buildoutput|jspm_packages|ThirdParty)$',
    \ 'file': '\v\.(exe|dll|obj|zip|suo|jpg|bmp|png)$',
    \ }

"neocomplete plugin
let g:neocomplete#enable_at_startup = 1

autocmd FileType javascript setlocal omnifunc=javascriptcomplete#CompleteJS

"vim-javascript plugin
let g:javascript_plugin_flow = 1
let g:javascript_plugin_jsdoc = 1

" vimwiki stuff
" Run multiple wikis "
let g:vimwiki_list = [
        \{'path': '~/Documents/VimWiki/work.wiki', 'syntax': 'markdown', 'ext': '.markdown'},
        \{'path': '~/Documents/VimWiki/personal.wiki', 'syntax': 'markdown', 'ext': '.markdown'}
\]
au BufRead,BufNewFile *.wiki set filetype=vimwiki
:autocmd FileType vimwiki map <buffer> <leader>d :VimwikiMakeDiaryNot<CR>
function! ToggleCalendar()
  execute ":Calendar"
  if exists("g:calendar_open")
    if g:calendar_open == 1
      execute "q"
      unlet g:calendar_open
    else
      g:calendar_open = 1
    end
  else
    let g:calendar_open = 1
  end
endfunction
:autocmd FileType vimwiki map <buffer> <leader>c :call ToggleCalendar()<CR>
