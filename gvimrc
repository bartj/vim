silent! colorscheme molokai
set guioptions-=T   " get rid of the toolbar
set guioptions-=m   " get rid of the menu
set guioptions+=b   " display the horizontal scroll bar
set guioptions+=c   " use console dialogs

if has("gui_gtk2")
    set guifont=Inconsolata\ 10
else
    set guifont=Consolas:h10
endif
